package com.example.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.model.Empleado;

@Repository
public interface EmpleadoDao extends JpaRepository<Empleado, Integer>{
	@Query(value = "{call guardar_empleado(:p_apellido, :s_apellido, :p_nombre, :s_nombre, :t_documento, :n_documento, :f_ingres, :area1, :pais)}", nativeQuery= true)
	public String guardarEmpleado(
			@Param("p_apellido") String p_apellido,
			@Param("s_apellido") String s_apellido,
			@Param("p_nombre") String p_nombre,
			@Param("s_nombre") String s_nombre,
			@Param("t_documento") String t_documento,
			@Param("n_documento") String n_documento,
			@Param("f_ingres") String f_ingres,
			@Param("area1") String area1,
			@Param("pais") String pais);
	
	@Query(value = "SELECT COUNT(*) FROM empleado WHERE p_apellido LIKE :p_apellido AND s_apellido LIKE :s_apellido AND p_nombre LIKE :p_nombre "
			+ "AND s_nombre LIKE :s_nombre AND pais LIKE :pais AND t_documento LIKE :t_documento AND n_documento LIKE :n_documento "
			+ "AND email LIKE :email AND estado LIKE :estado", nativeQuery = true)
    public String listaCantidad(
    		@Param("p_apellido") String p_apellido,
			@Param("s_apellido") String s_apellido,
			@Param("p_nombre") String p_nombre,
			@Param("s_nombre") String s_nombre,
			@Param("pais") String pais,
			@Param("t_documento") String t_documento,
			@Param("n_documento") String n_documento,
			@Param("email") String email,
			@Param("estado") String estado);
	
	@Query(value = "SELECT * FROM empleado WHERE p_apellido LIKE :p_apellido AND s_apellido LIKE :s_apellido AND p_nombre LIKE :p_nombre "
			+ "AND s_nombre LIKE :s_nombre AND pais LIKE :pais AND t_documento LIKE :t_documento AND n_documento LIKE :n_documento "
			+ "AND email LIKE :email AND estado LIKE :estado LIMIT :primero,:ultimo", nativeQuery = true)
    public List<Empleado> lista(
    		@Param("p_apellido") String p_apellido,
			@Param("s_apellido") String s_apellido,
			@Param("p_nombre") String p_nombre,
			@Param("s_nombre") String s_nombre,
			@Param("pais") String pais,
			@Param("t_documento") String t_documento,
			@Param("n_documento") String n_documento,
			@Param("email") String email,
			@Param("estado") String estado,
			@Param("primero") int primero,
			@Param("ultimo") int ultimo);
	
	@Query(value = "{call editar_empleado(:id, :p_apellido, :s_apellido, :p_nombre, :s_nombre, :t_documento, :n_documento, :f_ingres, :area1, :pais, :email)}", nativeQuery= true)
	public String editarEmpleado(
			@Param("id") String id,
			@Param("p_apellido") String p_apellido,
			@Param("s_apellido") String s_apellido,
			@Param("p_nombre") String p_nombre,
			@Param("s_nombre") String s_nombre,
			@Param("t_documento") String t_documento,
			@Param("n_documento") String n_documento,
			@Param("f_ingres") String f_ingres,
			@Param("area1") String area1,
			@Param("pais") String pais,
			@Param("email") String email);

}
