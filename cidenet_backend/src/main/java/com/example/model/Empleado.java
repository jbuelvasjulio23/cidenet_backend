package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Empleado {
	
	@Id
	private Integer id;
	
	@Column
	private String p_nombre;
	
	@Column
	private String s_nombre;
	
	@Column
	private String p_apellido;
	
	@Column
	private String s_apellido;
	
	@Column
	private String pais;
	
	@Column
	private String t_documento;
	
	@Column
	private String n_documento;
	
	@Column
	private String email;
	
	@Column
	private String f_ingreso;
	
	@Column
	private String area;
	
	@Column
	private String estado;
	
	@Column
	private String f_hora_registro;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getP_nombre() {
		return p_nombre;
	}

	public void setP_nombre(String p_nombre) {
		this.p_nombre = p_nombre;
	}

	public String getS_nombre() {
		return s_nombre;
	}

	public void setS_nombre(String s_nombre) {
		this.s_nombre = s_nombre;
	}

	public String getP_apellido() {
		return p_apellido;
	}

	public void setP_apellido(String p_apellido) {
		this.p_apellido = p_apellido;
	}

	public String getS_apellido() {
		return s_apellido;
	}

	public void setS_apellido(String s_apellido) {
		this.s_apellido = s_apellido;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getT_documento() {
		return t_documento;
	}

	public void setT_documento(String t_documento) {
		this.t_documento = t_documento;
	}

	public String getN_documento() {
		return n_documento;
	}

	public void setN_documento(String n_documento) {
		this.n_documento = n_documento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getF_ingreso() {
		return f_ingreso;
	}

	public void setF_ingreso(String f_ingreso) {
		this.f_ingreso = f_ingreso;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getF_hora_registro() {
		return f_hora_registro;
	}

	public void setF_hora_registro(String f_hora_registro) {
		this.f_hora_registro = f_hora_registro;
	}

	
}
