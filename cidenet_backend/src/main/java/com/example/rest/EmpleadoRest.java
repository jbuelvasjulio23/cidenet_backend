package com.example.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.dao.EmpleadoDao;
import com.example.model.Empleado;
import com.example.service.EmpleadoService;

@RestController
@RequestMapping("empleados")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE})
public class EmpleadoRest {
	@Autowired
	private EmpleadoService empleadoService;
	
	@PostMapping("/guardar")
	public ResponseEntity<Empleado> guardarEmpleado(@RequestBody Empleado empleado) {
		Empleado emp = new Empleado();
		emp.setEmail(empleadoService.guardarEmpleado(empleado));
		return new ResponseEntity<>(emp, HttpStatus.OK); 
	}
	
	@PostMapping("/mostrarCantidad")
	public ResponseEntity<Empleado> listaCantidad(@RequestBody Empleado empleado) {
		Empleado emp = new Empleado();
		emp.setEmail(empleadoService.listaCantidad(empleado));
		return new ResponseEntity<>(emp, HttpStatus.OK);
	}
	
	@PostMapping("/mostrar/{primero}/{ultimo}")
	public ResponseEntity<List<Empleado>> lista(@RequestBody Empleado empleado,@PathVariable("primero")int prim,@PathVariable("ultimo")int ult) {
		List<Empleado> lista = empleadoService.lista(empleado, prim, ult);
		return new ResponseEntity(lista, HttpStatus.OK);
	}
	
	@DeleteMapping("/borrar/{id}")
    public ResponseEntity<?> borrar(@PathVariable("id")int id){
		empleadoService.eliminarEmpleado(id);
		Empleado emp = new Empleado();
		emp.setEmail("Empleado eliminado");
        return new ResponseEntity(emp, HttpStatus.OK);
    }
	
	@PostMapping("/editar")
	public ResponseEntity<Empleado> editarEmpleado(@RequestBody Empleado empleado) {
		Empleado emp = new Empleado();
		emp.setEmail(empleadoService.editarEmpleado(empleado));
		return new ResponseEntity<>(emp, HttpStatus.OK); 
	}
}
