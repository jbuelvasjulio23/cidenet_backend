package com.example.service;

public class Extra {
	
	public static String paisIs(String n) {
		switch (n) {
		case "1":
			return "Colombia";

		case "2":
			return "Estados unidos";

		default:
			return "error pais";
		}
	}
	
	public static String documentoIs(String n) {
		switch (n) {
		case "1":
			return "Cedula de ciudadania";

		case "2":
			return "Cedula de extranjeria";

		case "3":
			return "Pasaporte";

		case "4":
			return "Permiso especial";

		default:
			return "error tipo documento";
		}
	}
	
	public static String areaIs(String n) {
		switch (n) {
		case "1":
			return "Administracion";

		case "2":
			return "Financiera";

		case "3":
			return "Compras";

		case "4":
			return "Infraestructura";

		case "5":
			return "Operacion";

		case "6":
			return "Talento humano";

		case "7":
			return "Servicios varios";

		default:
			return "error area";
		}
	}
	
	public static String estadoIs(String n) {
		switch (n) {
		case "1":
			return "Activo";

		case "2":
			return "Inactivo";

		default:
			return "error estado";
		}
	}
}
