package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.EmpleadoDao;
import com.example.model.Empleado;

@Service
public class EmpleadoService {
	@Autowired
	private EmpleadoDao empleadoDao;

	public String guardarEmpleado(Empleado empleado) {
		String pais = "";
		String t_docume = "";
		String area;

		pais = Extra.paisIs(empleado.getPais());
		if (pais.equals("error pais")) {
			return pais;
		}

		t_docume = Extra.documentoIs(empleado.getT_documento());
		if (t_docume.equals("error tipo documento")) {
			return t_docume;
		}

		area = Extra.areaIs(empleado.getArea());
		if (area.equals("error area")) {
			return area;
		}

		return empleadoDao.guardarEmpleado(empleado.getP_apellido(), empleado.getS_apellido(), empleado.getP_nombre(),
				empleado.getS_nombre(), t_docume, empleado.getN_documento(), empleado.getF_ingreso(), area, pais);
	}
	
	public String listaCantidad(Empleado empleado) {
		String pais = "";
		String t_docume = "";
		String estado = "";

		if (!empleado.getPais().equals("")) {
			pais = Extra.paisIs(empleado.getPais());
			System.out.println("pais: " + empleado.getPais());
			if (pais.equals("error pais")) {
				return null;
			}
		}

		if (!empleado.getT_documento().equals("")) {
			t_docume = Extra.documentoIs(empleado.getT_documento());
			System.out.println("t_docume: " + t_docume);
			if (t_docume.equals("error tipo documento")) {
				return null;
			}
		}

		if (!empleado.getEstado().equals("")) {
			estado = Extra.estadoIs(empleado.getEstado());
			System.out.println("estado: " + estado);
			if (estado.equals("error estado")) {
				return null;
			}
		}

		return empleadoDao.listaCantidad(empleado.getP_apellido(), empleado.getS_apellido(), empleado.getP_nombre(),
				empleado.getS_nombre(), pais + "%", t_docume + "%", empleado.getN_documento(), empleado.getEmail(),
				estado + "%");
	}

	public List<Empleado> lista(Empleado empleado, int primero, int ultimo) {
		String pais = "";
		String t_docume = "";
		String estado = "";

		if (!empleado.getPais().equals("")) {
			pais = Extra.paisIs(empleado.getPais());
			System.out.println("pais: " + empleado.getPais());
			if (pais.equals("error pais")) {
				return null;
			}
		}

		if (!empleado.getT_documento().equals("")) {
			t_docume = Extra.documentoIs(empleado.getT_documento());
			System.out.println("t_docume: " + t_docume);
			if (t_docume.equals("error tipo documento")) {
				return null;
			}
		}

		if (!empleado.getEstado().equals("")) {
			estado = Extra.estadoIs(empleado.getEstado());
			System.out.println("estado: " + estado);
			if (estado.equals("error estado")) {
				return null;
			}
		}
		System.out.println("lista de empleados");

		return empleadoDao.lista(empleado.getP_apellido(), empleado.getS_apellido(), empleado.getP_nombre(),
				empleado.getS_nombre(), pais + "%", t_docume + "%", empleado.getN_documento(), empleado.getEmail(),
				estado + "%", primero, ultimo);
	}
	
	public void eliminarEmpleado(int id) {
		empleadoDao.deleteById(id);
	}
	
	public String editarEmpleado(Empleado empleado) {
		String pais = "";
		String t_docume = "";
		String area;

		pais = Extra.paisIs(empleado.getPais());
		if (pais.equals("error pais")) {
			return pais;
		}

		t_docume = Extra.documentoIs(empleado.getT_documento());
		if (t_docume.equals("error tipo documento")) {
			return t_docume;
		}

		area = Extra.areaIs(empleado.getArea());
		if (area.equals("error area")) {
			return area;
		}

		return empleadoDao.editarEmpleado(empleado.getId()+"", empleado.getP_apellido(), empleado.getS_apellido(), empleado.getP_nombre(),
				empleado.getS_nombre(), t_docume, empleado.getN_documento(), empleado.getF_ingreso(), area, pais, empleado.getEmail());
	}
}
